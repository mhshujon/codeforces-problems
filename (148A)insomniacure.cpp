//
// Created by mhshujon on 7/4/20.
// url : https://codeforces.com/problemset/problem/148/A
//
#include <iostream>
#include <math.h>
using namespace std;

int main(){
    int d, k, l, m, n, count = 0;
    cin >> k;
    cin >> l;
    cin >> m;
    cin >> n;
    cin >> d;

    if (d >= 1 && d <= pow(10, 5 )){
        if (k >=1 && k <= 10 && l >=1 && l <= 10 && m >=1 && m <= 10 && n >=1 && n <= 10){
            if (k == 1 || l == 1 || m == 1 || n == 1){
                cout << d;
            } else {
                for (int i = 1; i <= d; i++) {
                    if (i % k != 0 && i % l != 0 && i % m != 0 && i % n != 0) count++;
                }
                cout << d - count;
            }
        }
    }

    return 0;
}