//
// Created by mhshujon on 9/7/20.
// url : https://codeforces.com/problemset/problem/199/A
//
#include <iostream>
using namespace std;

int main(){
    int n;
    cin >> n;

    cout << 0 << ' ' << 0 << ' ' << n << endl;

    return 0;
}