//
// Created by mhshujon on 9/1/20.
// url : http://codeforces.com/problemset/problem/34/A
//
#include <iostream>
using namespace std;

int main(){
    int n;
    cin >> n;
    int heights[n], indexOne, indexTwo, minHeight, heightDiff;

    for (int i = 0; i < n; i++) {
        cin >> heights[i];
    }

    heightDiff = heights[0] - heights[n-1];
    if (heightDiff < 0) heightDiff *= (-1);
    minHeight = heightDiff;
    indexOne = 1;
    indexTwo = n;

    for (int i = 0; i < n-1; i++) {
        heightDiff = heights[i] - heights[i+1];
        if (heightDiff < 0) heightDiff *= (-1);

        if (minHeight > heightDiff){
            minHeight = heightDiff;
            indexOne = i + 1;
            indexTwo = i + 2;
        }
    }

    cout << indexOne << ' ' << indexTwo << endl;

    return 0;
}