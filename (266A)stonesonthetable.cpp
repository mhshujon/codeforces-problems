//
// Created by mhshujon on 7/1/20.
// url : https://codeforces.com/contest/266/problem/A
//
#include <iostream>
#include <string>
using namespace std;

int main() {
    int n;
    cin >> n;
    string stones;
    cin >> stones;
    char temp = 'N';
    int count = 0;
    int flag = 1;

    if (n >= 1 && n <= 50){
        for (int i = 0; i < n; i++) {
            if (stones[i] == 'R' || stones[i] == 'G' || stones[i] == 'B'){
                if (temp == 'N'){
                    temp = stones[i];
                } else if (temp == stones[i]){
                    count++;
                    temp = stones[i];
                } else temp = stones[i];
            }
            else {
                flag = 0;
                break;
            }
        }
        if (flag == 1)
            cout << count;
    }

    return 0;
}