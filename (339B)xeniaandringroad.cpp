//
// Created by mhshujon on 9/8/20.
// url : https://codeforces.com/problemset/problem/339/B
//
#include <iostream>
using namespace std;

int main() {
    int n, m, position(1), current;
    long long int moves(0);

    cin >> n >> m;

    for (int i = 0; i < m; ++i) {
        cin >> current;

        if (current >= position)
            moves += current - position;
        else
            moves += n - position + current;

        position = current;
    }

    cout << moves;

    return 0;
}