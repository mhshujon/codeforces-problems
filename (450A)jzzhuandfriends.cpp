//
// Created by mhshujon on 7/9/20.
// url : https://codeforces.com/problemset/problem/450/A
//
#include <iostream>
#include <math.h>
using namespace std;

int main(){
    int n, m, index;
    double temp, max(0);
    cin >> n >> m;
    int i;

    for (i = 1; i <= n; i++) {
        cin >> temp;
        if (temp > m) {
            if (max <= ceil(temp / m)) {
                max = ceil(temp / m);
                index = i;
            }
        }
    }

    if (max == 0) index = i - 1;

    cout << index;

    return 0;
}