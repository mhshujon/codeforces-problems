//
// Created by mhshujon on 7/3/20.
// url : https://codeforces.com/problemset/problem/110/A
//
#include <iostream>
#include <string>
#include <math.h>
using namespace std;

int main() {
    int count = 0;
    string number;
    cin >> number;
    int length = number.length();

    if (length >= 1 && length <= pow(10, 18)){
        for (int i = 0; i < length; i++) {
            if (number[i] == '4' || number[i] == '7') {
                count++;
            }
        }
        if (count == 4 || count == 7)
            cout << "YES";
        else cout << "NO";
    }

    return 0;
}