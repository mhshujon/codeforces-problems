//
// Created by mhshujon on 7/6/20.
// url : https://codeforces.com/problemset/problem/155/As
//
#include <iostream>
#include <math.h>
using namespace std;

int main(){
    int contest, min, max, count(0);
    cin >> contest;
    int points[contest];

    for (int i = 0; i < contest; i++) {
        cin >> points[i];
    }

    min = max = points[0];

    for (int i = 0; i < contest-1; i++) {
        if (points[i] <= pow(10, 4)){
            if (points[i+1] > points[i] && points[i+1] > max){
                max = points[i+1];
                count++;
            }
            if (points[i+1] < points[i] && points[i+1] < min){
                min = points[i+1];
                count++;
            }
        }
    }

    cout << count;

    return 0;
}