//
// Created by mhshujon on 7/3/20.
// url : https://codeforces.com/problemset/problem/80/A
//
#include <iostream>
using namespace std;

int main(){
    int num1, num2, nextPrime;
    bool flag1 = true, flag2 = false;

    cin >> num1 >> num2;

    if (num1 >=2 && num1 <= 50 && num1 < num2){
        for (int i = 2; i <= num1 / 2; i++) {
            if (num1 % i == 0) {
                cout << "NO";
                flag1 = false;
                break;
            }
        }
        if (flag1){
            nextPrime = num1 + 1;
            for (int j = 2; j <= nextPrime / 2; j++) {
                if(nextPrime % j == 0) {
                    nextPrime++;
                    j = 2;
                }
            }
        }
        if (nextPrime == num2)
            cout << "YES";
        else {
            cout << "NO";
        }
    }

    return 0;
}