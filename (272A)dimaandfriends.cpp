//
// Created by mhshujon on 7/9/20.
// url : https://codeforces.com/problemset/problem/272/A
//
#include <iostream>
using namespace std;

int main(){

    int n, finger, tfingers(0), dima(0);
    cin >> n;

    for (int i = 0; i < n; ++i) {
        cin >> finger;
        tfingers += finger;
    }

    for (int i = 1; i <= 5; ++i) {
        if ((tfingers+i) % (n+1) != 1) dima++;
    }

    cout << dima;

    return 0;
}