//
// Created by mhshujon on 6/28/2020.
// url : https://codeforces.com/problemset/problem/4/C (still unsolved, time limit exceeded on test 7)
//
#include <iostream>
#include <map>
using namespace std;

int main() {
    int n;
    cin >> n;

    map<string,int> db;
    string newUser;

    while (n--){
        cin >> newUser;
        if (db.find(newUser) == db.end()) {
            db.insert(pair<string,int>(newUser,0));
            cout << "OK" << endl;
        } else
            cout << newUser << ++db[newUser] << endl;
    }

    return 0;
}