//
// Created by mhshujon on 7/4/20.
// url : https://codeforces.com/problemset/problem/200/B
//
#include <iostream>
using namespace std;

int main(){
    int n;
    cin >> n;
    float coctail[n], sum = 0.0;
    bool flag = true;

    if (n >=1 && n <= 100){
        for (int i = 0; i < n; i++) {
            cin >> coctail[i];
            if (coctail[i] >=0 && coctail[i] <=100){
                sum += coctail[i];
            } else {
                flag = false;
                break;
            }
        }
        if (flag) cout << sum / n;

    }

    return 0;
}