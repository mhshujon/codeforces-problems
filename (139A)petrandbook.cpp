//
// Created by mhshujon on 9/1/20.
// url : http://codeforces.com/problemset/problem/139/A
//
#include <iostream>
using namespace std;

int main(){
    int n, pg[7], pgRead(0), weekDay;

    cin >> n;

    for (int i = 0; i < 7; i++) {
        cin >> pg[i];
    }
    int i(0);
    while (true){
        pgRead += pg[i];

        i++;

        if (i >= 7 && pgRead < n) {
            i = 0;
        }
        else if (pgRead >= n){
            weekDay = i;
            break;
        }
    }

    cout << weekDay << endl;

    return 0;
}
