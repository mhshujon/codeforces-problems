//
// Created by mhshujon on 7/4/20.
// url : https://codeforces.com/problemset/problem/144/A
//
#include <iostream>
using namespace std;

int main(){
    int n;
    cin >> n;
    int height[n], max = 0, min = 101, maxP, minP;

    if (n >=2 && n <= 100){
        for (int i = 0; i < n; i++) {
            cin >> height[i];
            if (height[i] > max){
                max = height[i];
                maxP = i;
            }

            if (height[i] <= min){
                min = height[i];
                minP = i;
            }
        }

        if (minP < maxP) minP++;
        cout << maxP + (n-1) - minP;

    }

    return 0;
}