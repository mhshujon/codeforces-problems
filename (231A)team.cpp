//
// Created by mhshujon on 6/29/20.
// url : https://codeforces.com/contest/231/problem/A
//
#include <iostream>
#include <string>
using namespace std;

int main() {
    int n;
    cin >> n;
    int decisions[n][3];
    int yes = 0, willSolve = 0;

    if(n >= 1 && n <= 1000){
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                cin >> decisions[i][j];
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                if (decisions[i][j] == 0 || decisions[i][j] == 1){
                    if (decisions[i][j] == 1){
                        yes += 1;
                        if (yes == 2)
                            break;
                    }
                }
            }
            if (yes >= 2) {
                willSolve += 1;
            }
            yes = 0;
        }

        cout << willSolve;
    }

    return 0;
}