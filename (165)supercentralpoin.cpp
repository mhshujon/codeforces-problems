//
// Created by mhshujon on 8/28/20.
// url : http://codeforces.com/contest/165/problem/A
//
#include <iostream>
using namespace std;

int main(){
    int n, sc(0);
    cin >> n;
    int cp[n][2];

    for (int i = 0; i < n; i++) {
        cin >> cp[i][0] >> cp[i][1];
    }

    for (int i = 0; i < n; i++) {
        bool left = false, right = false, upper = false, lower = false;
        for (int j = 0; j < n; j++) {
            if (cp[i][0] > cp[j][0]) {
                if (cp[i][1] == cp[j][1]) left = true;
            }
            if (cp[i][0] < cp[j][0]) {
                if (cp[i][1] == cp[j][1]) right = true;
            }
            if (cp[i][0] == cp[j][0]) {
                if (cp[i][1] > cp[j][1]) lower = true;
            }
            if (cp[i][0] == cp[j][0]) {
                if (cp[i][1] < cp[j][1]) upper = true;
            }
        }
        if (left && right && upper && lower) {
            sc++;
        }
    }

    cout << sc << endl;

    return 0;
}