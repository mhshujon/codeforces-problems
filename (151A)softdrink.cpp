//
// Created by mhshujon on 7/6/20.
// url : https://codeforces.com/problemset/problem/151/A
//
#include <iostream>
#include <string>
#include <math.h>
using namespace std;

int main(){
    int n, k, l, c, d, p, nl, np, min = 1000;
    cin >> n >> k >> l >>  c >> d >> p >> nl >> np;

    if (n >=1 && n <= 1000 && k >=1 && k <= 1000 && l >=1 && l <= 1000 && c >=1 && c <= 1000 && d >=1 && d <= 1000 && p >=1 && p <= 1000 && nl >=1 && nl <= 1000 && np >=1 && np <= 1000) {
        int drinks = k * l;
        int toast = drinks / nl, slice = c * d, salt = p / np;

        if (min > toast) min = toast;
        if (min > slice) min = slice;
        if (min > salt) min = salt;

        cout << min / n;
    }

    return 0;
}