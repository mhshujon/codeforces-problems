//
// Created by mhshujon on 6/3/20.
// url : https://codeforces.com/contest/233/problem/A
//
#include <iostream>
using namespace std;

int main(){
    int num;
    string output;
    cin >> num;
    bool flag = true;

    if (num >= 1 && num <=100){
        if (num == 1) flag = false;
        else {
            for (int i = 1; i <= num; i++) {
                if (i % 2 != 0) {
                    if (i-1 == 0) output += to_string(i+1);
                    else if (i+1 <= num) output += ' ' + to_string(i+1);
                    else flag = false;
                }
                else {
                    if (i-1 == 0) output += to_string(i-1);
                    else if (i-1 <= num) output += ' ' + to_string(i-1);
                    else cout << false;
                }
            }
        }
        if (flag) cout << output;
        else cout << -1;
    }

    return 0;
}