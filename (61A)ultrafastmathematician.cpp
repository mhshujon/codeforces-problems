//
// Created by mhshujon on 7/3/20.
// url : https://codeforces.com/problemset/problem/61/A
//
#include <iostream>
using namespace std;

int main(){
    string num1, num2;
    cin >> num1;
    cin >> num2;
    string output;

    if (num1.length() > 0 && num1.length() == num2.length()){
        for (int i = 0; i < num1.length(); i++) {
            if (num1[i] == num2[i])
                output += '0';
            else output += '1';
        }
        cout << output;
    }

    return 0;
}