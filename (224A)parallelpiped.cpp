//
// Created by mhshujon on 9/3/20.
// url : http://codeforces.com/problemset/problem/224/A
//
#include <iostream>
#include <math.h>
using namespace std;

int main(){
    int edges[3];

    for (int i = 0; i < 3; i++) {
        cin >> edges[i];
    }

    int result = 4 * (sqrt((edges[0] * edges[2]) / edges[1]) + sqrt((edges[0] * edges[1]) / edges[2]) + sqrt((edges[1] * edges[2]) / edges[0]));

    cout << result;

    return 0;
}