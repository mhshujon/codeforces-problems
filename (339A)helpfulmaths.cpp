//
// Created by mhshujon on 6/30/20.
// url : https://codeforces.com/contest/339/problem/A
//
#include <iostream>
using namespace std;
#include <cstring>

void rearrange(char db[], int length)
{
    char temp;

    for (int i = 0; i < length;) {
        for (int j = i+2; j < length;) {
            if (db[i] > db[j]){
                temp = db[i];
                db[i] = db[j];
                db[j] = temp;
            }
            j += 2;
        }
        i += 2;
    }
    for (int i = 0; i < length; i++) {
        cout << db[i];
    }
}

int main() {
    string sum;
    int flag = 0;
    cin >> sum;
    int length = sum.length();
    char temp[length];

    strcpy(temp, sum.c_str());


    if (length > 0 && length <= 100){
        for (int i = 0; i < length;) {
            int value = temp[i] - 48;
            if (value < 1 || value > 3){
                flag = 1;
                break;
            }
            i += 2;
        }
        if (flag == 0)
            rearrange(temp, length);
    }

    return 0;
}