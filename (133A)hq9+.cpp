//
// Created by mhshujon on 7/6/20.
// url : https://codeforces.com/contest/133/problem/A
//
#include <iostream>
#include <string>
using namespace std;

int main(){
    string input;
    cin >> input;

    if (input.length() >= 1 && input.length() <= 100) {
        if (input.find('H') >= 0 && input.find('H') < input.length() || input.find('Q') >= 0 && input.find('Q') < input.length() || input.find('9') >= 0 && input.find('9') < input.length())
            cout << "YES";
        else cout << "NO";
    }

    return 0;
}