//
// Created by mhshujon on 6/28/2020.
// url : https://codeforces.com/contest/112/problem/A
//
#include <iostream>
#include <string>
using namespace std;

int main() {
    string a;
    string b;

    cin >> a;
    cin >> b;

    int lna = a.length();
    int lnb = b.length();

    if (lna == lnb){
        for (int i = 0; i < lna; i++) {
            if(a[i] >= 65 && a[i] <= 90){
                a[i] = a[i] + 32;
            }
            if(b[i] >= 65 && b[i] <= 90){
                b[i] = b[i] + 32;
            }
        }

        cout << a.compare(b);

    }

    return 0;
}