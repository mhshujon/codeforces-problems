//
// Created by mhshujon on 9/8/20.
// url : https://codeforces.com/problemset/problem/352/A
//
#include <iostream>
using namespace std;

int main(){
    int n, fives(0), zeros(0), maxNum(0),input;
    cin >> n;

    for (int i = 0; i < n; i++) {
        cin >> input;
        if (input == 5) fives++;
        else if (input == 0) zeros++;
    }

    if (zeros == 0) cout << -1;
    else if (fives / 9 < 1) cout << 0;
    else{
        for (int i = 0; i < fives/9 * 9; ++i) {
            cout << 5;
        }
        for (int i = 0; i < zeros; ++i) {
            cout << 0;
        }
    }

    return 0;
}