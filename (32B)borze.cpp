//
// Created by mhshujon on 7/3/20.
// url : https://codeforces.com/problemset/problem/32/B
//
#include <iostream>
#include <string>
using namespace std;

int main() {
    int length;
    string s;
    cin >> s;
    length = s.length();
    for (int i = 0; i < length; i++) {
        if (s[i] == '.')
            cout << '0';
        else if (s[i] == '-' && s[i+1] == '.'){
            cout << '1';
            i++;
        }
        else if (s[i] == '-' && s[i+1] == '-') {
            cout << '2';
            i++;
        }
    }

    return 0;
}