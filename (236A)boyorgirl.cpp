//
// Created by mhshujon on 7/6/20.
// url : https://codeforces.com/contest/236/problem/A
//
#include <iostream>
#include <list>
using namespace std;

int main(){
    string username;
    cin >> username;
    list<char>distinct;
    list<char>::iterator it;
    bool flag = true;

    for (int i = 0; i < username.length(); i++) {
        if (distinct.empty()){
            distinct.push_back(username[i]);
        } else {
            for (it = distinct.begin(); it != distinct.end(); it++) {
                if (!flag) flag = true;
                if (*it == username[i]) {
                    flag = false;
                    break;
                }
            }
            if (flag) distinct.push_back(username[i]);
        }
    }

    if (distinct.size() % 2 != 0) cout << "IGNORE HIM!";
    else cout << "CHAT WITH HER!";

    return 0;
}