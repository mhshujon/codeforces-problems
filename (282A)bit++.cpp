//
// Created by mhshujon on 6/28/2020.
// url : https://codeforces.com/contest/282/problem/A
//
#include <iostream>
#include <string>
using namespace std;

int main() {
    int n;
    int x = 0;
    cin >> n;
    string statements[n];

    for (int i = 0; i < n; i++) {
        cin >> statements[i];
    }

    for (int i = 0; i < n; i++) {
        if (statements[i] == "x++" || statements[i] == "X++"){
            x++;
        }
        else if (statements[i] == "x--" || statements[i] == "X--"){
            x--;
        }
        else if (statements[i] == "++x" || statements[i] == "++X"){
            ++x;
        }
        else if (statements[i] == "--x" || statements[i] == "--X"){
            --x;
        }
    }

    cout << x << endl;

    return 0;
}