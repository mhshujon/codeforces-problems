//
// Created by mhshujon on 7/3/20.
// url : https://codeforces.com/problemset/problem/271/A
//
#include <iostream>
#include <string>
using namespace std;

int main() {
    int year;
    cin >> year;
    string syear = to_string(year);

    if (year >= 1000 && year <= 9000){
        while (true){
            year++;
            syear = to_string(year);
            if (syear[0] != syear[1] && syear[0] != syear[2] && syear[0] != syear[3]){
                if (syear[1] != syear[2] && syear[1] != syear[3]){
                    if (syear[2] != syear[3]){
                        break;
                    }
                }
            }
        }
        cout << year;
    }

    return 0;
}