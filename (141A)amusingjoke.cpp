//
// Created by mhshujon on 7/6/20.
// url : https://codeforces.com/problemset/problem/141/A
//
#include <iostream>
#include <string>
using namespace std;

int main(){
    string name1;
    cin >> name1;
    string name2;
    cin >> name2;
    string piles;
    cin >> piles;
    bool flag = true;
    string chr = "J";

    if (name1.length() >= 1 && name1.length() <= 100 && name2.length() >= 1 && name2.length() <= 100 && piles.length() >= 1 && piles.length() <= 100) {
        if (name1.length() + name2.length() == piles.length()) {
            for (int i = 0; i < name1.length(); i++) {
                chr = name1[i];
                if (piles.find(chr) >= 0 && piles.find(chr) < piles.length()) {
                    piles.erase(piles.find(chr), 1);
                } else {
                    flag = false;
                    break;
                }
            }
            if (flag){
                for (int i = 0; i < name2.length(); i++) {
                    chr = name2[i];
                    if (piles.find(chr) < 0 || piles.find(chr) > piles.length()) {
                        flag = false;
                        break;
                    }
                }
                if (flag) cout << "YES";
                else cout << "NO";
            } else cout << "NO";
        } else cout << "NO";
    }

    return 0;
}
