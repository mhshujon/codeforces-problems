//
// Created by mhshujon on 6/29/20.
// url : https://codeforces.com/contest/50/problem/A
//
#include <iostream>
using namespace std;

int main() {
    int m, n;
    cin >> m >> n;
    int boardSize = m * n;
    int givenSize = 2 * 1;

    if (n >= m && n >=1 && n <= 16 && m >=1 && m <= 16)
        cout << boardSize / givenSize;

    return 0;
}