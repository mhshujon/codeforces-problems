//
// Created by mhshujon on 7/1/20.
// url : https://codeforces.com/problemset/problem/69/A
//
#include <iostream>
#include <string>
#include <cstring>
using namespace std;

int main() {
    int n;
    cin >> n;
    int forces[n][3];
    int sum = 0;
    int flag = 1;

    if (n >= 1 && n <= 100){
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                cin >> forces[i][j];
            }
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (forces[j][i] >= -100 && forces[j][i] <= 100){
                    sum += forces[j][i];
                } else{
                    flag = 0;
                    break;
                }
            }
            if (sum != 0){
                flag = 0;
                cout << "NO";
                break;
            }
        }

        if (flag == 1){
            cout << "YES";
        }
    }

    return 0;
}