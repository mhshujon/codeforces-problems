//
// Created by mhshujon on 7/3/20.
// url : https://codeforces.com/problemset/problem/59/A
//
#include <iostream>
#include <string>
#include <cstring>
using namespace std;

int main() {
    string s;
    cin >> s;
    int uCount = 0, lCount = 0;
    int length = s.length();

    if (length >=1 && length <=100){
        for (int i = 0; i < length; ++i) {
            if (s[i] >= 'A' && s[i] <= 'Z')
                uCount++;
            else lCount++;
        }
        for (int i = 0; i < length; ++i) {
            if (uCount > lCount){
                if (s[i] >= 'a' && s[i] <= 'z')
                    s[i] -= 32;
            } else{
                if (s[i] >= 'A' && s[i] <= 'Z')
                    s[i] += 32;
            }
        }
    }

    cout << s;

    return 0;
}