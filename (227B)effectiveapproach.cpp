//
// Created by mhshujon on 7/8/20.
// url : https://codeforces.com/problemset/problem/227/B
//
#include <iostream>
using namespace std;

int main(){
    int n, m, a, b, index[100001];
    long long v(0), p(0);
    cin >> n;

    for (int i = 1; i <= n; i++) {
        cin >> a;
        index[a] = i;
    }

    cin >> m;

    while(m--){
        cin >> b;
        v += index[b];
        p += (n+1) - index[b];
    }

    cout << v << ' ' << p;

    return 0;
}