//
// Created by mhshujon on 6/28/2020.
// url : https://codeforces.com/contest/71/problem/A
//
#include <iostream>
#include <string>
using namespace std;

int main() {
    int n;
    cin >> n;
    string strArr[n];

    if (n >= 1 && n <= 100){
        for (int i = 0; i < n; i++) {
            cin >> strArr[i];
        }
        for (int i = 0; i < n; i++) {
            int strln = strArr[i].length();
            if (strln > 10){
                strArr[i].replace(1, strln-2, to_string(strln-2));
                cout << strArr[i] << endl;
            } else cout << strArr[i] << endl;
        }
    }

    return 0;
}