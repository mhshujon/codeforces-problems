//
// Created by mhshujon on 6/30/20.
// url : https://codeforces.com/contest/281/problem/A
//
#include <iostream>
using namespace std;
#include <string>

int main() {
    string input;
    cin >> input;
    int length = input.length();

    if (length >= 1 && length <= 1000){
        if (input[0] >= 'a' && input[0] <= 'z')
            input[0] = input[0] - 32;
    }
    cout << input;

    return 0;
}