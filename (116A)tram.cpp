//
// Created by mhshujon on 7/6/20.
// url : https://codeforces.com/problemset/problem/116/A
//
#include <iostream>
using namespace std;

int main(){
    int stops, count(0), capacity = 0;
    cin >> stops;
    int passengers[stops][2];
    bool  flag = true;

    if (stops >= 2 && stops <= 1000){
        for (int i = 0; i < stops; i++) {
            cin >> passengers[i][0];
            cin >> passengers[i][1];
        }

        for (int i = 0; i < stops; ++i) {
            if (passengers[0][0] != 0 || passengers[stops - 1][1] != 0 || passengers[i][0] < 0 || passengers[i][0] > 1000 || passengers[i][1] < 0 || passengers[i][1] > 1000) {
                flag = false ;
                break;
            } else {
                count -= passengers[i][0];
                count += passengers[i][1];
                if (capacity < count) capacity = count;
            }
        }

        if (flag) cout << capacity;
    }

    return 0;
}