//
// Created by mhshujon on 6/28/2020.
// url : https://codeforces.com/contest/158/problem/A
//
#include <iostream>
#include <string>
using namespace std;

int main() {
    int n;
    int k;

    cin >> n >> k;

    if ((n >= 1 && n <= 50) && (n >= k)){
        int scores[n];

        for (int i = 0; i < n; i++) {
            cin >> scores[i];
        }

        int kScore = scores[k-1];
        int totalCount = 0;

        for (int j = 0; j < n; j++) {
            if (scores[j] >= kScore && scores[j] != 0)
                totalCount += 1;
        }

        cout <<totalCount << endl;
    }

    return 0;
}