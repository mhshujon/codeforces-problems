//
// Created by mhshujon on 7/3/20.
// url : https://codeforces.com/problemset/problem/275/A
//
#include <iostream>
using namespace std;

int main() {
    int dboard[3][3], input[3][3];

    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            dboard[i][j] = 1;
        }
    }

    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            cin >> input[i][j];
        }
    }

    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            if(input[i][j] >= 0 && input[i][j] <=100){
                if (input[i][j] % 2 != 0){
                    if (i==0 && j==0){
                        if (dboard[0][0] == 0)
                            dboard[0][0] = 1;
                        else dboard[0][0] = 0;

                        if (dboard[0][1] == 0)
                            dboard[0][1] = 1;
                        else dboard[0][1] = 0;

                        if (dboard[1][0] == 0)
                            dboard[1][0] = 1;
                        else dboard[1][0] = 0;
                    }
                    else if (i==0 && j==1){
                        if (dboard[0][0] == 0)
                            dboard[0][0] = 1;
                        else dboard[0][0] = 0;

                        if (dboard[0][1] == 0)
                            dboard[0][1] = 1;
                        else dboard[0][1] = 0;

                        if (dboard[0][2] == 0)
                            dboard[0][2] = 1;
                        else dboard[0][2] = 0;

                        if (dboard[1][1] == 0)
                            dboard[1][1] = 1;
                        else dboard[1][1] = 0;
                    }
                    else if (i==0 && j==2){
                        if (dboard[0][2] == 0)
                            dboard[0][2] = 1;
                        else dboard[0][2] = 0;

                        if (dboard[0][1] == 0)
                            dboard[0][1] = 1;
                        else dboard[0][1] = 0;

                        if (dboard[1][2] == 0)
                            dboard[1][2] = 1;
                        else dboard[1][2] = 0;
                    }
                    else if (i==1 && j==0){
                        if (dboard[1][0] == 0)
                            dboard[1][0] = 1;
                        else dboard[1][0] = 0;

                        if (dboard[0][0] == 0)
                            dboard[0][0] = 1;
                        else dboard[0][0] = 0;

                        if (dboard[2][0] == 0)
                            dboard[2][0] = 1;
                        else dboard[2][0] = 0;

                        if (dboard[1][1] == 0)
                            dboard[1][1] = 1;
                        else dboard[1][1] = 0;
                    }
                    else if (i==1 && j==1){
                        if (dboard[1][1] == 0)
                            dboard[1][1] = 1;
                        else dboard[1][1] = 0;

                        if (dboard[0][1] == 0)
                            dboard[0][1] = 1;
                        else dboard[0][1] = 0;

                        if (dboard[2][1] == 0)
                            dboard[2][1] = 1;
                        else dboard[2][1] = 0;

                        if (dboard[1][0] == 0)
                            dboard[1][0] = 1;
                        else dboard[1][0] = 0;

                        if (dboard[1][2] == 0)
                            dboard[1][2] = 1;
                        else dboard[1][2] = 0;
                    }
                    else if (i==1 && j==2){
                        if (dboard[1][2] == 0)
                            dboard[1][2] = 1;
                        else dboard[1][2] = 0;

                        if (dboard[1][1] == 0)
                            dboard[1][1] = 1;
                        else dboard[1][1] = 0;

                        if (dboard[0][2] == 0)
                            dboard[0][2] = 1;
                        else dboard[0][2] = 0;

                        if (dboard[2][2] == 0)
                            dboard[2][2] = 1;
                        else dboard[2][2] = 0;
                    }
                    else if (i==2 && j==0){
                        if (dboard[2][0] == 0)
                            dboard[2][0] = 1;
                        else dboard[2][0] = 0;

                        if (dboard[1][0] == 0)
                            dboard[1][0] = 1;
                        else dboard[1][0] = 0;

                        if (dboard[2][1] == 0)
                            dboard[2][1] = 1;
                        else dboard[2][1] = 0;
                    }
                    else if (i==2 && j==1){
                        if (dboard[2][1] == 0)
                            dboard[2][1] = 1;
                        else dboard[2][1] = 0;

                        if (dboard[2][0] == 0)
                            dboard[2][0] = 1;
                        else dboard[2][0] = 0;

                        if (dboard[2][2] == 0)
                            dboard[2][2] = 1;
                        else dboard[2][2] = 0;

                        if (dboard[1][1] == 0)
                            dboard[1][1] = 1;
                        else dboard[1][1] = 0;
                    }
                    else if (i==2 && j==2){
                        if (dboard[2][2] == 0)
                            dboard[2][2] = 1;
                        else dboard[2][2] = 0;

                        if (dboard[2][1] == 0)
                            dboard[2][1] = 1;
                        else dboard[2][1] = 0;

                        if (dboard[1][2] == 0)
                            dboard[1][2] = 1;
                        else dboard[1][2] = 0;
                    }
                }
            }
        }
    }

    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            cout << dboard[i][j];
        }
        if (i < 2)
            cout << endl;
    }

    return 0;
}