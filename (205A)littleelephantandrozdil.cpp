//
// Created by mhshujon on 9/7/20.
// url : https://codeforces.com/problemset/problem/205/A
//
#include <iostream>
using namespace std;

int main(){
    int n, minDist(1000000001), count(0), input, indexNum;
    cin >> n;

    for (int i = 1; i <= n; i++) {
        cin >> input;

        if (input < minDist){
            minDist = input;
            indexNum = i;
            count = 0;
        }
        else if (input == minDist){
            count++;
        }
    }

    if (count == 0) cout << indexNum << endl;
    else cout << "Still Rozdil" << endl;

    return 0;
}