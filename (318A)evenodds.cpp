//
// Created by mhshujon on 9/4/20.
// url : https://codeforces.com/problemset/problem/318/A
//
#include <iostream>
using namespace std;

int main(){
    long long m, n;

    cin >> m >> n;

    long long tempOne, tempTwo;

    tempOne = ((n-1) * 2) + 1;

    if (tempOne <= m)
        cout << tempOne;
    else {
        tempTwo = (((m + 1) / 2) - n) * 2;
        if (tempTwo < 0) cout << tempTwo * (-1);
        else cout << tempTwo;
    }

    return 0;
}