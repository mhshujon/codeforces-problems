//
// Created by mhshujon on 7/6/20.
// url : https://codeforces.com/problemset/problem/228/A
//
#include <iostream>
#include <list>
using namespace std;

int main(){
    int shoes[4], count(0);
    list <int> duplicates;
    list<int> :: iterator k;
    bool flag = true;

    for (int i = 0; i < 4; i++) {
        cin >> shoes[i];
    }

    for (int i = 0; i < 4; i++) {
        for (int j = 1; j < 4; j++) {
            if (shoes[j] == shoes[i] && i != j){
                if (duplicates.empty()){
                    duplicates.push_back(shoes[j]);
                } else {
                    for (k = duplicates.begin(); k != duplicates.end(); k++) {
                        if (*k == shoes[j]) {
                            flag = false;
                            break;
                        }
                    }
                    if (flag) duplicates.push_back(shoes[j]);
                }
            }
        }
    }

    for (k = duplicates.begin(); k != duplicates.end(); k++) {
        for (int i = 0; i < 4; i++) {
            if (*k == shoes[i]) count++;
        }
    }

    cout << count - duplicates.size();

    return 0;
}