//
// Created by mhshujon on 7/2/20.
// url :https://codeforces.com/problemset/problem/266/B
//
#include <iostream>
#include <string>
#include <cstring>
using namespace std;

int main() {
    int n, t, length;
    cin >> n >> t;
    string s;

    if (n >= 1 && n <= 50){
        if (t >= 1 && t <= 50){
            cin >> s;
            length = s.size();

            if (length == n){
                int i = 0;
                while (i < t){
                    for (int i = 0; i < length-1; i++) {
                        if (s[i] == 'B' || s[i] == 'G' || s[i+1] == 'B' || s[i+1] == 'G' ){
                            if (s[i] == 'B' && s[i+1] == 'G'){
                                s[i] = 'G';
                                s[i+1] = 'B';
                                i++;
                            }
                        }
                    }
                    i++;
                }
                cout << s;
            }
        }
    }

    return 0;
}